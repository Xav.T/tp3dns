#!/usr/bin/env bash

#Config pour wiki.org

himage dwikiorg mkdir -p /etc/named
hcp dwikiorg/named.conf dwikiorg:/etc/.
hcp dwikiorg/* dwikiorg:/etc/named/.
himage dwikiorg rm /etc/named/named.conf 

#Config pour drtiutre.rt.iut.re

himage drtiutre mkdir -p /etc/named
hcp drtiutre/named.conf drtiutre:/etc/.
hcp drtiutre/named/* drtiutre:/etc/named/.

#Config pour diutre.iut.re

himage diutre mkdir -p /etc/named
hcp diutre/named.conf diutre:/etc/.
hcp diutre/* diutre:/etc/named/.

#Config pour aRootServer

himage aRootServer mkdir -p /etc/named
hcp aRootServer/named.conf aRootServer:/etc/.
hcp aRootServer/* aRootServer:/etc/named/.
himage aRootServer rm /etc/named/named.conf 

#Config pour dorg.org

himage dorg mkdir -p /etc/named
hcp dorg/named.conf dorg:/etc/.
hcp dorg/* dorg:/etc/named/.
himage dorg rm /etc/named/named.conf 

#Config pour dre.re

himage re mkdir -p /etc/named
hcp re/named.conf dre:/etc/.
hcp re/* dre:/etc/named/.
himage dre rm /etc/named/named.conf 